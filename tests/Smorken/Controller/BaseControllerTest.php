<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 7:40 AM
 */

use Smorken\Controller\BaseController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Mockery as m;

class BaseControllerTest extends Orchestra\Testbench\TestCase {

    /**
     * @var BaseController
     */
    protected $sut;

    protected $ctrl = 'Mockery_0_Smorken_Controller_BaseController';

    public function setUp()
    {
        parent::setUp();

        $this->app['config']->set('smorken/controller::config.layout', 'smorken/controller::testing.layout');
        $this->sut = m::mock('Smorken\Controller\BaseController')
            ->shouldDeferMissing()
            ->shouldAllowMockingProtectedMethods();
        $this->sut->shouldReceive('__construct')
            ->andReturnNull();
        $this->sut->setBase('testing');
        $this->sut->setSubPackage('smorken/controller::');
        $this->app->instance($this->ctrl, $this->sut);
        Route::controller('base', $this->ctrl);
    }

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    protected function getPackageProviders()
    {
        return array(
            'Smorken\Controller\ControllerServiceProvider',
        );
    }

    /**
     * @expectedException Smorken\Controller\ControllerException
     */
    public function testControllerThrowsNoProvider()
    {
        $this->action('GET', $this->ctrl . '@getIndex');
    }


    public function testGetIndex()
    {
        $provider = m::mock('StdClass');
        $provider->shouldReceive('all')
            ->once()
            ->andReturn(array());
        $this->sut->setProvider($provider);
        $response = $this->action('GET', $this->ctrl . '@getIndex');
        $this->assertResponseOk();
    }

    /**
     * @expectedException Smorken\Controller\ControllerException
     */
    public function testPostIndex()
    {
        $this->action('POST', $this->ctrl . '@postIndex');
    }

    public function testGetView()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $provider->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($model);
        $this->sut->setProvider($provider);
        $this->action('GET', $this->ctrl . '@getView', array('id' => 1));
        $this->assertResponseOk();
    }

    /**
     * @expectedException Smorken\Controller\ControllerException
     */
    public function testPostView()
    {
        $this->action('POST', $this->ctrl . '@postView', array('id' => 1));
    }

    /**
     * @expectedException ErrorException
     */
    public function testGetCreateThrowsViewNotFound()
    {
        $provider = $this->mockSimple();
        $provider->shouldReceive('getModel')
            ->once()
            ->andReturn($this->mockSimple());
        $this->sut->setProvider($provider);
        $this->sut->setSubPackage(false);
        $this->action('GET', $this->ctrl . '@getCreate');
    }

    public function testGetCreate()
    {
        $provider = $this->mockSimple();
        $provider->shouldReceive('getModel')
            ->once()
            ->andReturn($this->mockSimple());
        $this->sut->setProvider($provider);
        $this->action('GET', $this->ctrl . '@getCreate');
        $this->assertResponseOk();
    }

    /**
     * @expectedException Smorken\Controller\ControllerException
     */
    public function testPostCreate()
    {
        $this->action('POST', $this->ctrl . '@postCreate');
    }

    /**
     * @expectedException ErrorException
     */
    public function testGetUpdateThrowsViewNotFound()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $this->sut->setProvider($provider);
        $provider->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($model);
        $provider->shouldReceive('getModel')
            ->never();
        $this->sut->setSubPackage(false);
        $this->action('GET', $this->ctrl . '@getUpdate', array('id' => 1));
    }

    public function testGetUpdate()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $provider->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($model);
        $provider->shouldReceive('getModel')
            ->never();
        $this->sut->setProvider($provider);
        $this->action('GET', $this->ctrl . '@getUpdate', array('id' => 1));
        $this->assertResponseOk();
    }

    /**
     * @expectedException Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testLoadModelThrows404()
    {
        $provider = $this->mockSimple();
        $provider->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturnNull();
        $provider->shouldReceive('getModel')
            ->never();
        $this->sut->setProvider($provider);
        $this->sut->setBase('smorken/controller::testing');
        $this->action('GET', $this->ctrl . '@getUpdate', array('id' => 1));
        $this->assertResponseOk();
    }

    /**
     * @expectedException Smorken\Controller\ControllerException
     */
    public function testPostUpdate()
    {
        $this->action('POST', $this->ctrl . '@postUpdate', array('id' => 1));
    }


    public function testGetDelete()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $provider->shouldReceive('find')
            ->once()
            ->with(1)
            ->andReturn($model);
        $provider->shouldReceive('getModel')
            ->never();
        $this->sut->setProvider($provider);
        $this->sut->setBase('smorken/controller::testing');
        $this->action('GET', $this->ctrl . '@getDelete', array('id' => 1));
        $this->assertResponseOk();
    }

    public function testDeleteDelete()
    {
        $provider = $this->mockSimple();
        $provider->shouldReceive('delete')
            ->once()
            ->with(1)
            ->andReturn(true);
        $provider->shouldReceive('getModel')->never();
        $provider->shouldReceive('find')->never();
        $this->sut->setProvider($provider);
        $this->action('DELETE', $this->ctrl . '@deleteDelete', array('id' => 1));
        $this->assertSessionHas('success');
        $this->assertRedirectedToAction($this->ctrl . '@getIndex');
    }

    public function testDeleteDeleteFails()
    {
        $provider = $this->mockSimple();
        $provider->shouldReceive('delete')
            ->once()
            ->with(1)
            ->andReturn(false);
        $provider->shouldReceive('getModel')->never();
        $provider->shouldReceive('find')->never();
        $this->sut->setProvider($provider);
        $this->action('DELETE', $this->ctrl . '@deleteDelete', array('id' => 1));
        $this->assertSessionHas('warning');
        $this->assertRedirectedToAction($this->ctrl . '@getIndex');
    }

    public function testHandlePostSaveRedirects()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $provider->shouldReceive('getModel')
            ->once()
            ->andReturn($model);
        $provider->shouldReceive('errors')
            ->once()
            ->andReturn(false);
        $provider->shouldReceive('name')
            ->once()
            ->with($model)
            ->andReturn('foo');

        $this->sut->setProvider($provider);
        $this->sut->handlePostSaveRedirects($provider);
        $this->assertSessionHas('success');
    }

    public function testHandlePostSaveRedirectsErrors()
    {
        $model = $this->mockSimple();
        $provider = $this->mockSimple();
        $provider->shouldReceive('getModel')
            ->once()
            ->andReturn($model);
        $provider->shouldReceive('errors')
            ->twice()
            ->andReturn(array('errors'));
        $provider->shouldReceive('id')
            ->once()
            ->with($model)
            ->andReturn(1);

        $this->sut->setProvider($provider);
        $this->sut->handlePostSaveRedirects($provider);
        $this->assertSessionHasErrors();
    }

    public function mockSimple()
    {
        $mocked = m::mock('StdClass');
        return $mocked;
    }
} 