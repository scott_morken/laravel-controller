<h3>The following record(s) will be deleted:</h3>
<div class="well">
    {{ $model }}
</div>
{!! Form::open(array('method' => 'delete')) !!}
<div class="button-group">
{!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
<a href="{{ action(Smorken\Controller\Helpers\RouteHelper::controllerClass() . '@getIndex') }}" title="Cancel delete" class="btn btn-primary">Cancel</a>
</div>
{!! Form::close() !!}