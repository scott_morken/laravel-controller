<?php namespace Smorken\Controller;

use Illuminate\Support\ServiceProvider;

class ControllerServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->loadViewsFrom(__DIR__ . '/../views/common', 'smorken/controller');
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path() . '/vendor/smorken/controller/config.php',
        ], 'config');
	}

    protected function registerResources()
    {
        $userconfigfile = config_path() . '/vendor/smorken/controller/config.php';
        $packageconfigfile = __DIR__ . '/../config/config.php';
        $config = $this->app['files']->getRequire($packageconfigfile);
        if (file_exists($userconfigfile)) {
            $uconfig = $this->app['files']->getRequire($userconfigfile);
            $config = array_replace_recursive($config, $uconfig);
        }
        $this->app['config']->set('smorken/controller::config', $config);
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->registerResources();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
