<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 6:58 AM
 */

namespace Smorken\Controller;

use Closure;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class BaseController extends Controller {

    /**
     * @var string master layout template
     */
    protected $master;
    /**
     * @var string package name that goes with $base views
     */
    protected $package;
    /**
     * @var string base path name of view that controller uses ('base')
     */
    protected $base = '';
    /**
     * @var string Subnav lookup string
     */
    protected $subnav = '';
    /**
     * @var bool $paged use paged index or standard index
     */
    protected $paged = false;
    /**
     * @var object provider for controller
     */
    protected $provider;

    public function __construct()
    {
        $this->init();
    }

    public function getIndex()
    {
        if ($this->paged) {
            return $this->getIndexPaged();
        }
        else {
            return $this->getIndexAll();
        }
    }

    protected function getIndexPaged()
    {
        $models = $this->getProvider()->paginate();
        view()->share('operations', static::ops());
        return view($this->getViewName('index'))
            ->with('models', $models);
    }

    protected function getIndexAll()
    {
        $models = $this->getProvider()->all();
        view()->share('operations', static::ops());
        return view($this->getViewName('index'))
            ->with('models', $models);
    }

    public function postIndex()
    {
        throw new ControllerException("Not implemented.");
    }

    public function getView($id)
    {
        return $this->handleCommonGets('view', true, $id);
    }

    public function postView($id)
    {
        throw new ControllerException("Not implemented.");
    }

    public function getCreate()
    {
        return $this->handleCommonGets('create', false);
    }

    public function postCreate()
    {
        throw new ControllerException("Not implemented.");
    }

    public function getUpdate($id)
    {
        return $this->handleCommonGets('update', true, $id);
    }

    public function postUpdate($id)
    {
        throw new ControllerException("Not implemented.");
    }

    public function getDelete($id)
    {
        return $this->handleCommonGets('delete', true, $id);
    }

    public function deleteDelete($id)
    {
        if ($this->getProvider()->delete($this->loadModel($id))) {
            session()->flash('success', "Resource with id #$id deleted.");
        }
        else {
            session()->flash('danger', $id . ' NOT deleted.');
        }
        return redirect()->action($this->getRoute('getIndex'));
    }

    protected function postCreateDefault(array $attributes)
    {
        $model = $this->getProvider()->create($attributes);
        $this->getProvider()->setModel($model);
        return $this->handlePostSaveRedirects($this->getProvider());
    }

    protected function postUpdateDefault($id, array $attributes)
    {
        $model = $this->loadModel($id);
        $this->getProvider()->update($model, $attributes);
        $this->getProvider()->setModel($model);
        return $this->handlePostSaveRedirects($this->getProvider());
    }

    public function handlePostSaveRedirects($provider)
    {
        $model = $provider->getModel();
        if (!$provider->errors()) {
            session()->flash('success', $provider->name($model) . " saved.");
            return redirect()->action($this->getRoute('getIndex'));
        }
        if ($model) {
            return redirect()->action($this->getRoute('getUpdate'), array('id' => $provider->id($model)))
                ->withInput()
                ->withErrors($provider->errors());
        }
        else {
            return redirect()->action($this->getRoute('getCreate'))
                ->withInput()
                ->withErrors($provider->errors());
        }
    }

    protected function handleCommonGets($action, $idreq = true, $id = null)
    {
        if ($idreq || $id) {
            $model = $this->loadModel($id);
            view()->share('operations', static::ops($id));
        }
        else {
            $model = $this->getProvider()->getModel();
            view()->share('operations', static::ops());
        }
        return view($this->getViewName($action, true))
            ->with('model', $model)
            ->with('base', $this->getViewName());
    }

    public function loadModel($id)
    {
        $model = $this->getProvider()->find($id);
        if (!$model) {
            abort(404, 'Resource not found.');
        }
        return $model;
    }

    public static function ops($id = null)
    {
        $ops = array(
            'Create' => array('url' => action(static::getRoute('getCreate'))),
            'List' => array('url' => action(static::getRoute('getIndex'))),
        );
        if ($id) {
            $ops = array_merge($ops, static::opsWithId($id));
        }
        return $ops;
    }

    public static function opsWithId($id, $show_text = true)
    {
        return array(
            'View' => array(
                'url' => action(static::getRoute('getView'), array('id' => $id)),
                'icon' => config('smorken/controller::config.icons.view', 'glyphicon glyphicon-eye-open'),
                'show_text' => $show_text,
            ),
            'Update' => array(
                'url' => action(static::getRoute('getUpdate'), array('id' => $id)),
                'icon' => config('smorken/controller::config.icons.update', 'glyphicon glyphicon-pencil'),
                'show_text' => $show_text,
            ),
            'Delete' => array(
                'url' => action(static::getRoute('getDelete'), array('id' => $id)),
                'icon' => config('smorken/controller::config.icons.delete', 'glyphicon glyphicon-trash'),
                'show_text' => $show_text,
            ),
        );
    }

    public static function opsStyle()
    {
        return 'list-unstyled';
    }

    public function init()
    {
        $this->setupMaster();
        $this->initSubnav();
    }

    /**
     * Setup the master template.
     *
     * @return void
     */
    protected function setupMaster()
    {
        if (!$this->master) {
            $this->loadMasterFromConfig();
        }
        if ($this->master) {
            view()->share('master', $this->master);
        }
    }

    protected function loadMasterFromConfig()
    {
        if ($this->package) {
            $subpath = $this->package . 'config.master';
            $l = config($subpath, null);
            if ($l) {
                $this->setMaster($l);
                return true;
            }
        }
        $l = config('smorken/controller::config.master', 'layouts.master');
        if ($l) {
            $this->setMaster($l);
            return true;
        }
        return false;
    }

    protected static function getRoute($action)
    {
        $called = get_called_class();
        if (substr($called, 0, 1) !== '\\') {
            $called = '\\' . $called;
        }
        return $called . '@' . $action;
    }

    protected function getViewName($view = null)
    {
        $viewname = '';
        if ($this->package) {
            $viewname = $this->package;
        }
        $viewname .= $this->base;
        if (substr($viewname, -1) !== '.' && $view) {
            $viewname .= '.';
        }
        return $viewname . $view;
    }

    /**
     * @param string $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @param string $layout
     */
    public function setMaster($layout)
    {
        $this->master = $layout;
    }

    /**
     * @param string $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @throws ControllerException
     * @return object
     */
    protected function getProvider()
    {
        if (!$this->provider) {
            throw new ControllerException("A provider must be set to use this controller.");
        }
        return $this->provider;
    }

    /**
     * @param object $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param string $subnav
     */
    public function setSubnav($subnav)
    {
        $this->subnav = $subnav;
        $this->initSubnav();
    }

    protected function initSubnav()
    {
        if ($this->subnav) {
            view()->share('sub', $this->subnav);
        }
    }


    /**
     * Parse the given filter and options.
     *
     * @param  \Closure|string  $filter
     * @param  array  $options
     * @return array
     */
    protected function parseFilter($filter, array $options)
    {
        $original = parent::parseFilter($filter, $options);
        if (isset($options['parameters'])) {
            $original['parameters'] = array_merge($original['parameters'], $options['parameters']);
        }
        return $original;
    }
} 