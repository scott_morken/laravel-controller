<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/23/14
 * Time: 1:52 PM
 */

namespace Smorken\Controller\Helpers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

class RouteHelper {

    public static function controllerClass()
    {
        $routeArray = Str::parseCallback(Route::currentRouteAction(), null);

        if (last($routeArray) != null) {
            $controller = head($routeArray);
            return $controller;
        }

        return null;
    }
} 