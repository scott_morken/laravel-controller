<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/24/14
 * Time: 7:00 AM
 */
return array(
    'master' => 'smorken/views::layouts.master',
    'icons' => array(
        'view' => 'glyphicon glyphicon-eye-open',
        'update' => 'glyphicon glyphicon-pencil',
        'delete' => 'glyphicon glyphicon-trash',
    )
);